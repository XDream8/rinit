use nix::sys::signal::{kill, Signal};
use nix::unistd::Pid;

fn main() {
    let args: Vec<String> = std::env::args().collect();
    match args.get(0).map(|s| s.as_str()) {
        Some("poweroff") => {
            let _ = kill(Pid::from_raw(1), Signal::SIGUSR1);
        }
        Some("reboot") => {
            let _ = kill(Pid::from_raw(1), Signal::SIGINT);
        }
        _ => {
            eprintln!("Invalid command");
            std::process::exit(1);
        }
    }
}
