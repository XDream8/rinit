use nix::sys::signal::{kill, SigAction, SigHandler, Signal};
use nix::unistd::Pid;
use std::process::Command;
use std::thread;
use std::time::Duration;

fn main() {
    // Exit with status 1 if the process ID is not 1
    if nix::unistd::getpid().as_raw() != 1 {
        std::process::exit(1);
    }

    // Run the boot script
    Command::new("/lib/init/rc.boot")
        .status()
        .expect("Failed to execute rc.boot");

    // Add signal handlers for poweroff and reboot
    let poweroff_action = SigAction::new(
        SigHandler::Handler(poweroff_handler),
        nix::sys::signal::SaFlags::empty(),
        nix::sys::signal::SigSet::empty(),
    );
    unsafe {
        nix::sys::signal::sigaction(Signal::SIGUSR1, &poweroff_action)
            .expect("Failed to set signal action");
    }

    let reboot_action = SigAction::new(
        SigHandler::Handler(reboot_handler),
        nix::sys::signal::SaFlags::empty(),
        nix::sys::signal::SigSet::empty(),
    );
    unsafe {
        nix::sys::signal::sigaction(Signal::SIGINT, &reboot_action)
            .expect("Failed to set signal action");
    }

    // Sleep for a day (86400 seconds)
    loop {
        thread::sleep(Duration::from_secs(86400));
    }
}

extern "C" fn poweroff_handler(_: nix::libc::c_int) {
    Command::new("/lib/init/rc.shutdown")
        .arg("poweroff")
        .status()
        .expect("Failed to execute rc.shutdown");
}

extern "C" fn reboot_handler(_: nix::libc::c_int) {
    Command::new("/lib/init/rc.shutdown")
        .arg("reboot")
        .status()
        .expect("Failed to execute rc.shutdown");
    let _ = kill(Pid::from_raw(1), Signal::SIGINT);
}
