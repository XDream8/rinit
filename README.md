<div align="center">
    <h1>rinit</h1>
    <p>An implementation of shinit init system in rust</p>
</div>

## building from git source
```sh
$ git clone https://codeberg.org/XDream8/rinit
$ cd rinit
$ cargo build --profile optimized
```

## installation
```sh
$ install -Dm755 "target/optimized/rinit" "/usr/bin/rinit"
$ install -Dm755 "target/optimized/poweroff" "/usr/bin/poweroff"
$ ln -sf "/usr/bin/rinit" "/usr/bin/init"
$ ln -sf "/usr/bin/poweroff" "/usr/bin/reboot"
```
